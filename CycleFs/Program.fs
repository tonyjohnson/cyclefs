﻿
open System
open FSharp.Control.Reactive
open System.Reactive.Subjects

module Core =
    type Driver<'Out, 'In> = private Driver of (ISubject<'Out> * 'In)

    type Sink = private Subscription of (unit -> unit)

    let sink o (Driver (subj, _)) : Sink =
        Sink.Subscription (fun () -> o |> Observable.add subj.OnNext)

    let source (Driver (_, source)) = source

    let driver f = 
        let sinkProxy = new ReplaySubject<_>()
        let source = f(sinkProxy :> IObservable<_>)
        Driver(sinkProxy :> ISubject<_>, source)

    let run (main: 'Drivers -> Sink list) (drivers: 'Drivers) =
        let sinks = main drivers
        sinks |> List.iter (fun (Sink.Subscription f) -> f())
        
[<RequireQualifiedAccess>]
module Console =
    type Command =
        | WriteLine of string
        | ReadLine

    type Driver = Core.Driver<Command list, IObservable<string>>

    let private handleCommand = function
        | WriteLine s -> 
            Console.WriteLine s
            None
        | ReadLine -> Console.ReadLine() |> Some

    let private driver commands =
        commands |> Observable.flatmapSeq (List.choose handleCommand >> Seq.ofList)

    let makeDriver() = driver |> Core.driver

module Sample =
    type Drivers = {
        Console: Console.Driver }

    let main (drivers: Drivers) =
        let intZ =  
            drivers.Console
            |> Core.source
            |> Observable.map Int32.Parse

        let outputZ =
            intZ
            |> Observable.startWith [0]
            |> Observable.map (fun  i ->
                [sprintf "Received %i" i |> Console.WriteLine; Console.ReadLine])
                
        [drivers.Console |> Core.sink outputZ]

[<EntryPoint>]
let main args = 
    let drivers: Sample.Drivers = { Console = Console.makeDriver() }
    Core.run Sample.main drivers

    System.Console.WriteLine "Finished......"
    System.Console.ReadLine() |> ignore
    0